package leetcode;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class TwoSumTest {

  @Test
  void twoSum() {
    //given: an unordered array and a target sum
    int[] inputArray = new int[]{5, 7, 2, 3};
    int targetSum =  9;

    //when: the two sum function is called
    TwoSum twoSum = new TwoSum();
    int[] result = twoSum.twoSum(inputArray, targetSum);

    //then: the result should be two indices for which the sum of those numbers are equal to target
    int[] expected = new int[] {1, 2};
    assertArrayEquals(expected, result);
    assertThat(result, equalTo(expected));
  }

  @Test
  void twoSumSort() {
    //TODO
  }
}