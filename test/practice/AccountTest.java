package practice;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class AccountTest {
    private double epsilon = 1e-6;

    @Test
    public void accountCannotHaveNegativeOverdraftLimit() {
        Account account = new Account(-20);
        assertEquals(0d, account.getOverdraftLimit(), epsilon);
    }

    @Test
    public void accountCannotOverstepOverdraftLimit() {
        Account account = new Account(20);
        assertFalse(account.withdraw(21));
    }

    @Test
    public void depositCannotAcceptNegativeValue() {
        Account account = new Account(20);
        assertFalse(account.deposit(-1));
    }

    @Test
    public void withdrawnCannotAcceptNegativeValue() {
        Account account = new Account(20);
        assertFalse(account.withdraw(-1));
    }

    @Test
    public void depositCorrectAmount() {
        Account account = new Account(20);
        double amount = 2000;
        assertTrue(account.deposit(amount));
        assertEquals(account.getBalance(), amount, epsilon);
    }

    @Test
    public void withdrawnCorrectAmount() {
        Account account = new Account(20);
        double amount = 10;
        assertTrue(account.withdraw(amount));
        assertEquals(account.getBalance(), -amount, epsilon);
    }

    @Test
    public void depositAndWithdrawnCorrectAmount() {
        Account account = new Account(20);
        assertTrue(account.deposit(2200));
        assertTrue(account.withdraw(200));
        assertEquals(account.getBalance(), 2000d, epsilon);
    }
}