package outsys;

public class PowerOf2 {

  // 4 / 2 = 2 / 2 = 1

  public static boolean isPowerOf2(int n) {
    while (n>1) n /= 2;
    return n == 1;
  }

  public static void main(String[] args) {
    System.out.println(isPowerOf2(16));
  }

  //1.5 / 2 = 0
}
