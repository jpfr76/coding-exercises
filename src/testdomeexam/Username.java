package testdomeexam;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Username {
    public static boolean validate(String username) {
        if (username.length() < 6 || username.length() > 16) return false;
        Pattern pattern = Pattern.compile("^([a-zA-Z]+[a-zA-Z0-9]+(-[a-zA-Z0-9]+)?)$");
        Matcher matcher = pattern.matcher(username);
        return matcher.find();
    }

    public static void main(String[] args) {
        System.out.println(validate("Mike-Standish")); // Valid username
        System.out.println(validate("Mike Standish")); // Invalid username
    }
}