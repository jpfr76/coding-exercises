package testdomeexam;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MovingTotal {
    List<Integer> queue;
    Set<Integer> totals;

    public MovingTotal() {
        queue = new ArrayList<>();
        totals = new HashSet<>();
    }
    /**
     * Adds/appends list of integers at the end of internal list.
     */
    public void append(int[] list) {
        for (int i : list) {
            queue.add(i);
            addTotal();
        }
    }

    private void addTotal() {
        int acc = 0;
        int len = queue.size();
        if (len < 3) return;
        for (int i = len - 1; i >=  len - 3; i--) {
            acc += queue.get(i);
        }
        totals.add(acc);
    }

    /**
     * Returns boolean representing if any three consecutive integers in the
     * internal list have given total.
     */
    public boolean contains(int total) {
        return totals.contains(total);
    }

    public static void main(String[] args) {
        MovingTotal movingTotal = new MovingTotal();

        movingTotal.append(new int[] { 1, 2, 3 });

        System.out.println(movingTotal.contains(6));
        System.out.println(movingTotal.contains(9));

        movingTotal.append(new int[] { 4 });

        System.out.println(movingTotal.contains(9));
    }
}