package hackerrank;

import static java.util.stream.Collectors.toList;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

class MudWallResult {

    /*
     * Complete the 'maxHeight' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER_ARRAY wallPositions
     *  2. INTEGER_ARRAY wallHeights
     */

    public static int maxHeight(List<Integer> wallPositions, List<Integer> wallHeights) {
        int nbrWalls = wallPositions.size();
        int damSize = wallPositions.get(nbrWalls - 1);
        int[] leftDamArray = new int[damSize];
        int[] rightDamArray = new int[damSize];
        int[] damArray = new int[damSize];

        for (int i = 0; i < nbrWalls; i++) {
            int wallPos = wallPositions.get(i);
            leftDamArray[wallPos - 1] = wallHeights.get(i);
            rightDamArray[wallPos - 1] = wallHeights.get(i);
        }

        for (int i = 1, j = damSize - 2; i < damSize; i++, j--) {
            if (leftDamArray[i - 1] > 0 && leftDamArray[i] == 0) {
                leftDamArray[i] = leftDamArray[i - 1] + 1;
            }
            if (rightDamArray[j] == 0 && rightDamArray[j + 1] > 0) {
                rightDamArray[j] = rightDamArray[j + 1] + 1;
            }
        }
        
        Set<Integer> wallPositionsSet = new HashSet<>(wallPositions);
        int maxMudHeight = 0;
        for (int i = 0 ; i < damSize; i++) {
            damArray[i] = Math.min(leftDamArray[i], rightDamArray[i]);
            if (damArray[i] > maxMudHeight && !wallPositionsSet.contains(i + 1)) {
                maxMudHeight = damArray[i];
            }
        }
        return maxMudHeight;
    }

}

public class MudWall {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));

        int wallPositionsCount = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> wallPositions = IntStream.range(0, wallPositionsCount).mapToObj(i -> {
            try {
                return bufferedReader.readLine().replaceAll("\\s+$", "");
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        })
            .map(String::trim)
            .map(Integer::parseInt)
            .collect(toList());

        int wallHeightsCount = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> wallHeights = IntStream.range(0, wallHeightsCount).mapToObj(i -> {
            try {
                return bufferedReader.readLine().replaceAll("\\s+$", "");
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        })
            .map(String::trim)
            .map(Integer::parseInt)
            .collect(toList());

        int result = MudWallResult.maxHeight(wallPositions, wallHeights);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
