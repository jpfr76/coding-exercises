package hackerrank;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Set;
import java.util.stream.Collectors;

public class TwoCharacters {

    // Complete the alternate function below.
    static int alternate(String s) {
        int maxSize = 0;
        Set<Integer> set = s.chars().boxed().collect(Collectors.toSet());

        for (int c1 : set) {
            for (int c2 : set) {
                if (c2 == c1) continue;
                String trial = "";
                for (char c : s.toCharArray()) {
                    if (c == ((char) c1) || c == ((char) c2)) trial += String.valueOf(c);
                }
                if (trial.length() > maxSize && isValid(trial)) {
                    maxSize = trial.length();
                }
            }
        }
        return maxSize;
    }

    private static boolean isValid(String s) {
        char[] chars = s.toCharArray();
        char prevChar = chars[0];
        for (int i = 1; i < chars.length; i++) {
            char c = chars[i];
            if (c == prevChar) return false;
            prevChar = chars[i];
        }
        return true;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));

        int l = Integer.parseInt(bufferedReader.readLine().trim());

        String s = bufferedReader.readLine();

        int result = alternate(s);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
