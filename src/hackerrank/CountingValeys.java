package hackerrank;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class CountingValeys {

    // Complete the countingValleys function below.
    static int countingValleys(int n, String s) {
        int level = 0;
        int nbrValleys = 0;
        boolean inValley = false;
        for (char c : s.toCharArray()) {
            if (c == 'D') {
                level--;
                if (level < 0) inValley = true;
            }
            else {
                level++;
                if (level >= 0 && inValley) {
                    inValley = false;
                    nbrValleys++;
                }
            }
        }
        return nbrValleys;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String s = scanner.nextLine();

        int result = countingValleys(n, s);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
