package hackerrank;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class MakingAnagram {

  // Complete the makeAnagram function below.
  static int makeAnagram(String a, String b) {
    int[] counters = new int['z' - 'a' + 1];
    for (char ca : a.toLowerCase().toCharArray()) {
      counters[ca - 'a']++;
    }
    for (char cb : b.toLowerCase().toCharArray()) {
      counters[cb - 'a']--;
    }
    return Arrays.stream(counters).reduce((i1, i2) -> Math.abs(i1) + Math.abs(i2)).getAsInt();
  }

  private static final Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) throws IOException {
    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));

    String a = scanner.nextLine();

    String b = scanner.nextLine();

    int res = makeAnagram(a, b);

    bufferedWriter.write(String.valueOf(res));
    bufferedWriter.newLine();

    bufferedWriter.close();

    scanner.close();
  }
}
