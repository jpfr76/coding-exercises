package hackerrank;

import static java.util.stream.Collectors.toList;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

class LargestSegmentResult {
  static final double PI = 3.14159265359;

  /*
   * Complete the 'largestSegment' function below.
   *
   * The function is expected to return a STRING.
   * The function accepts following parameters:
   *  1. INTEGER_ARRAY radius
   *  2. INTEGER numberOfSegments
   */

  public static String largestSegment(List<Integer> radius, int numberOfSegments) {
    double largestSegment = findLargestSegment(radius, numberOfSegments);
    DecimalFormat df = new DecimalFormat("#.0000");
    return df.format(largestSegment);
  }

  private static double findLargestSegment(List<Integer> radius, int numberOfSegments) {
    if (radius.isEmpty()) return 0;

    radius.sort((a, b) -> b - a);

    int nbrCircles = radius.size();

    if (nbrCircles > 1) {
      Set<Double> segments = new HashSet<>();
      for (int i = 1; i < numberOfSegments; i++) {
        for (int j = 0; j < nbrCircles; j++) {
          int r = radius.get(j);
          double area = (PI * r * r) / i;
          int currentNbrSegments = i;
          for (int k = 0; k < nbrCircles; k++) {
            if (j == k) {
              continue;
            }
            int r2 = radius.get(k);
            double area2 = PI * r2 * r2;
            if (area <= area2) {
              currentNbrSegments++;
            }
            if (currentNbrSegments == numberOfSegments) {
              segments.add(area);
              break;
            }
          }
        }
      }
      return Collections.max(segments);
    }

    int r = radius.get(0);
    return  (PI * r * r) / numberOfSegments;
  }
}

public class LargestSegment {
  public static void main(String[] args) throws IOException {
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));

    int radiusCount = Integer.parseInt(bufferedReader.readLine().trim());

    List<Integer> radius = IntStream.range(0, radiusCount).mapToObj(i -> {
      try {
        return bufferedReader.readLine().replaceAll("\\s+$", "");
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    })
        .map(String::trim)
        .map(Integer::parseInt)
        .collect(toList());

    int numberOfSegments = Integer.parseInt(bufferedReader.readLine().trim());

    String result = LargestSegmentResult.largestSegment(radius, numberOfSegments);

    bufferedWriter.write(result);
    bufferedWriter.newLine();

    bufferedReader.close();
    bufferedWriter.close();
  }
}
