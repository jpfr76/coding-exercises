package leetcode;

import java.util.Arrays;

class MoveZeros {
    public void moveZeroes2(int[] nums) {
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] != 0) {
                continue;
            }
            int j = i;
            do {
                int tmp = nums[j + 1];
                nums[j + 1] = nums[j];
                nums[j] = tmp;
                j++;
            } while (j < nums.length - 1);
        }
    }

    public void moveZeroes(int[] nums) {
        int j = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0) nums[j++] = nums[i];
        }
        for (; j < nums.length; j++) {
            nums[j] = 0;
        }
    }

    public static void main(String[] args) {
        int[] arr = new int[] { 0, 1, 0, 3, 12 };
        new MoveZeros().moveZeroes(arr);
        System.out.println(Arrays.toString(arr));
    }
}