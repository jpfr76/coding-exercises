package leetcode;

import java.util.Set;
import java.util.HashSet;

class FindDuplicates {
  public boolean containsDuplicate(int[] nums) {
    Set<Integer> set = new HashSet<>();
    for (int num : nums) {
      set.add(num);
    }
    return set.size() != nums.length;
  }
}