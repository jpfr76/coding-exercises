package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Intersection2Array {
    public int[] intersect(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        List<Integer> resultList = new ArrayList<>();
        int i = 0, j = 0;
        while (i < nums1.length && j < nums2.length) {
            if (nums1[i] < nums2[j]) {
                i++;
            } else if (nums1[i] > nums2[j]) {
                j++;
            } else {
                resultList.add(nums1[i]);
                i++; j++;
            }
        }
        return resultList.stream().mapToInt(n->n).toArray();
    }
}