package leetcode;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.stream.Collectors;

class PlusOne {
    public int[] plusOne(int[] digits) {
        String numStr = Arrays.stream(digits).mapToObj(Integer::toString).collect(Collectors.joining(""));
        BigInteger numPlusOne = new BigInteger(numStr).add(new BigInteger("1"));
        char[] plusOneChar = numPlusOne.toString().toCharArray();
        int[] result = new int[plusOneChar.length];
        for (int i = 0; i < plusOneChar.length; i++) {
            result[i] = Integer.parseInt(Character.toString(plusOneChar[i]));
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(new PlusOne().plusOne(new int[] {1, 2, 3})));
    }
}