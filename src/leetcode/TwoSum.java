package leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum {

  public int[] twoSum(int[] nums, int target) {
    Map<Integer, Integer> map = new HashMap<>();
    for (int i = 0; i < nums.length; i++) {
      int val = nums[i];
      map.put(val, i);
    }
    for (int i = 0; i < nums.length; i++) {
      int val = nums[i];
      int possibleVal = target - val;
      if (map.containsKey(possibleVal) && map.get(possibleVal) != i) {
        return new int[]{i, map.get(possibleVal)};
      }
    }
    return new int[]{-1, -1};
  }

  public int[] twoSumSorted(int[] nums, int target) {
    for (int i = 0; i < nums.length - 1; i++) {
      int val = nums[i];
      int possibleVal = target - val;
      int possibleValIdx = Arrays.binarySearch(nums, i + 1, nums.length, possibleVal);
      if (possibleValIdx > 0 && possibleValIdx != i) {
        return new int[]{i + 1, possibleValIdx + 1};
      }
    }
    return new int[]{-1, -1};
  }

  public static void main(String[] args) {
    System.out.println(
        Arrays.toString(new TwoSum().twoSumSorted(new int[]{1, 2, 3, 4, 4, 9, 56, 90}, 8)));
  }
}
