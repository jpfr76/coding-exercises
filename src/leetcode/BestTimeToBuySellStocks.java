package leetcode;

class BestTimeToBuySellStocks {
    public int maxProfit(int[] prices) {
        int profit = 0;
        for (int i = 0; i < prices.length - 1; i++) {
            int currentPrice = prices[i];
            int nextPrice = prices[i + 1];
            if (nextPrice > currentPrice) {
                profit += nextPrice - currentPrice;
            }
        }
        return profit;
    }
}