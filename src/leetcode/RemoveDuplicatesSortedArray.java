package leetcode;

import java.util.Arrays;

class RemoveDuplicatesSortedArray {

  public int removeDuplicates(int[] nums) {
    if (nums.length < 1) {
      return 0;
    }
    int j = 1;
    int len = 1;
    boolean distinct;
    for (int i = 1; i < nums.length; i++) {
      distinct = nums[i] != nums[i - 1];
      if (distinct) {
        nums[j] = nums[i];
        j++;
        len++;
      }
    }
    return len;
  }

  public static void main(String[] args) {
    int[] arr = new int[]{1, 1, 2, 3, 4, 4, 5, 5};
    System.out.println(new RemoveDuplicatesSortedArray().removeDuplicates(arr));
    System.out.println(Arrays.toString(arr));
  }
}