package leetcode;

class Palindrome {

  public boolean isPalindrome(String s) {
    char[] chars = s.toLowerCase().replaceAll("[^a-z0-9]", "").toCharArray();
    int i = 0, j = chars.length - 1;
    while (i < j) {
      if (chars[j--] != chars[i++]) {
        return false;
      }
    }
    return true;
  }

  public boolean isPalindrome2(String s) {
    if (s.length() == 0) {
      return true;
    }
    int lo = 0, hi = s.length() - 1;
    while (lo <= hi) {
      char lc = s.charAt(lo);
      char hc = s.charAt(hi);
      if (!Character.isLetterOrDigit(lc)) {
        lo++;
      } else if (!Character.isLetterOrDigit(hc)) {
        hi--;
      } else {
        if (Character.toLowerCase(lc) != Character.toLowerCase(hc)) {
          return false;
        }
        lo++;
        hi--;
      }
    }
    return true;
  }

  public static void main(String[] args) {
    System.out.println(new Palindrome().isPalindrome("A man, a 2 plan, a0 canal: Panama"));
  }
}