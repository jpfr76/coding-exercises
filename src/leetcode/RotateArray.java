package leetcode;

import java.util.Arrays;

class RotateArray {

  public void rotate(int[] nums, int k) {
    int[] result = new int[nums.length];
    for (int i = 0; i < nums.length; i++) {
      result[(i + k) % nums.length] = nums[i];
    }
    for (int i = 0; i < nums.length; i++) {
      nums[i] = result[i];
    }
  }

  public static void main(String[] args) {
    int[] arr = new int[]{1, 2, 3};
    new RotateArray().rotate(arr, 4);
    System.out.println(Arrays.toString(arr));
  }
}