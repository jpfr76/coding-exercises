package leetcode;

import java.util.HashMap;
import java.util.Map;

class RomanToInteger {
    public int romanToInt(String s) {
        Map<String, Integer> map = new HashMap<>();
        map.put("I", 1);
        map.put("IV", 4);
        map.put("V", 5);
        map.put("IX", 9);
        map.put("X", 10);
        map.put("XL", 40);
        map.put("L", 50);
        map.put("XC", 90);
        map.put("C", 100);
        map.put("CD", 400);
        map.put("D", 500);
        map.put("CM", 900);
        map.put("M", 1000);

        int result = 0;
        char[] characters = s.toCharArray();
        for (int i = 0; i < characters.length; i++) {
            Character c = characters[i];
            Character nextChar = null;
            if ((i + 1) < characters.length) {
                nextChar = characters[i + 1];
            }

            int nextNumber = map.get(Character.toString(c));
            if (nextChar != null && map.containsKey(c + nextChar.toString())) {
                nextNumber = map.get(c + nextChar.toString());
                i++;
            }
            result += nextNumber;
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(new RomanToInteger().romanToInt("III"));
        System.out.println(new RomanToInteger().romanToInt("IV"));
        System.out.println(new RomanToInteger().romanToInt("VIII"));
        System.out.println(new RomanToInteger().romanToInt("XV"));
        System.out.println(new RomanToInteger().romanToInt("XIV"));
        System.out.println(new RomanToInteger().romanToInt("IX"));
        System.out.println(new RomanToInteger().romanToInt("MCMXCIV"));
    }
}