package pramp;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

class FlattenDictionary {

  static HashMap<String, String> flattenDictionary(HashMap<String, Object> dict) {
    HashMap<String, String> result = new LinkedHashMap<>();
    flattenDictionaryAux(dict, result, "");
    return result;
  }

  static void flattenDictionaryAux(
      Map<String, Object> dict,
      Map<String, String> result,
      String parentKey
  ) {
    for (Map.Entry<String, Object> set : dict.entrySet()) {
      String key = set.getKey();
      Object obj = set.getValue();
      String finalKey = parentKey.equals("") ? key : key.equals("") ? parentKey : parentKey + "." + key;
      if (obj instanceof Integer || obj instanceof String) {
        result.put(finalKey, obj.toString());
      } else if (obj instanceof Map) {
        flattenDictionaryAux((Map<String, Object>) obj, result, finalKey);
      }
    }
  }

  public static void main(String[] args) {
    HashMap<String, Object> dict = new HashMap<>();
    dict.put("key1", 1);
    HashMap<String, Object> innerDict1 = new HashMap<>();
    innerDict1.put("a", 2);
    innerDict1.put("b", "3");
    HashMap<String, Object> innerDict2 = new HashMap<>();
    innerDict2.put("d", "3");
    HashMap<String, Object> innerDict3 = new HashMap<>();
    innerDict3.put("", "1");
    innerDict2.put("e", innerDict3);
    innerDict1.put("c", innerDict2);
    dict.put("key2", innerDict1);
    HashMap<String, String> flat = flattenDictionary(dict);
    flat.forEach((key, val) -> System.out.println(key + ":" + val));
  }

}