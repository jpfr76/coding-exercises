package pramp;

import java.util.Arrays;

public class ArrayArrayProducts {

  static int[] arrayOfArrayProducts(int[] arr) {
    int[] leftArray = new int[arr.length];
    int[] rightArray = new int[arr.length];
    int[] resultArray = new int[arr.length];

    if (arr.length <= 1) {
      return new int[0];
    }
    // arr = [2, 7, 3, 4]
    // leftArray = [2, 14, 42, 168]
    int product = 1;
    for (int i = 0; i < leftArray.length; i++) {
      product *= arr[i];
      leftArray[i] = product;
    }

    // arr = [2, 7, 3, 4]
    // rightArray = [168, 84, 12, 4]
    product = 1;
    for (int i = rightArray.length - 1; i >= 0; i--) {
      product *= arr[i];
      rightArray[i] = product;
    }

    // arr        = [  2,   7,    3,     4]
    // leftArray  = [  2,  14,   42,   168]
    // rightArray = [168,  84,   12,     4]
    resultArray[0] = rightArray[1];
    resultArray[resultArray.length - 1] = leftArray[leftArray.length - 2];
    for (int i = 1; i < resultArray.length - 1; i++) {
      resultArray[i] = leftArray[i - 1] * rightArray[i + 1];
    }

    return resultArray;
  }


  static int[] arrayOfArrayProducts2(int[] arr) {
    int[] leftArr = new int[arr.length];
    int[] rightArr = new int[arr.length];
    int[] resultArr = new int[arr.length];

    if (arr.length == 0 || arr.length == 1) {
      return new int[0];
    }

    // arr     = [ 2,   7,    3,     4 ]
    // leftArr = [ 1,   2,   14,    42 ]
    leftArr[0] = 1;
    for (int i = 0; i < leftArr.length - 1; i++) {
      leftArr[i + 1] = leftArr[i] * arr[i];
    }

    // arr      = [ 2 ,    7,    3,     4 ]
    // rightArr = [ 84,   12,    4,     1 ]
    rightArr[rightArr.length - 1] = 1;
    for (int j = rightArr.length - 1; j > 0; j--) {
      rightArr[j - 1] = rightArr[j] * arr[j];
    }

    // rightArr = [168, 84, 12, 1]
    for (int k = 0; k < arr.length; k++) {
      resultArr[k] = leftArr[k] * rightArr[k];
    }

    return resultArr;
  }


  public static void main(String[] args) {
    System.out.println(
        Arrays.toString(ArrayArrayProducts.arrayOfArrayProducts2(new int[]{2, 7, 3, 4})));
  }

}
