package codility;// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

import java.util.HashSet;
import java.util.Set;

class IntegerGap {
    public int solution(int[] A) {
        int maxN = 1000000;
        int len = A.length;
        int maxEl = 0;

        Set<Integer> set = new HashSet<>();

        for (int el : A) set.add(el);

        for (int i = 1; i < maxN && i <= len; i++) {
            if (!set.contains(i)) return i;
            if (A[i-1] > maxEl) maxEl = A[i-1];
        }

        return maxEl + 1;
    }
}