package codility;// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

import java.util.HashSet;
import java.util.Set;

class PermCheck {

    public int solution(int[] A) {
        Set<Integer> set = new HashSet<>();
        for (int item : A) set.add(item);
        for (int i = 1; i <= A.length; i++) if (!set.contains(i)) return 0;
        return 1;
    }

    public static void main(String[] args) {
        System.out.println(new PermCheck().solution(new int[] { 4, 1, 3, 2 }));
    }
}