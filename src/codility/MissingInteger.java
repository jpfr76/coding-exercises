package codility;

import java.util.HashSet;
import java.util.Set;

class MissingInteger {

  public int solution(int[] A) {
    int max = 0;
    Set<Integer> set = new HashSet<>();

    for (int item : A) {
      set.add(item);
      if (item > max) {
        max = item;
      }
    }
    for (int i = 1; i <= A.length; i++) {
      if (!set.contains(i)) {
        return i;
      }
    }

    return max + 1;
  }
}