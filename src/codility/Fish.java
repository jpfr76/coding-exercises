package codility;// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

import java.util.Stack;

class Fish {

  public int solution(int[] A, int[] B) {
    Stack<Integer> stack = new Stack<>();
    int survivals = B.length;
    for (int i = 0; i < B.length; i++) {
      if (B[i] == 1) {
        stack.push(A[i]);
      } else {
        while (!stack.isEmpty()) {
          int downstreamFishSize = stack.peek();
          int upstreamFishSize = A[i];
          survivals--;
          if (downstreamFishSize > upstreamFishSize) {
            break;
          } else {
            stack.pop();
          }
        }
      }
    }
    return survivals;
  }

  public static void main(String[] args) {
    System.out.println(new Fish().solution(new int[]{4, 3, 2, 1, 5}, new int[]{0, 1, 0, 0, 0}));
  }
}