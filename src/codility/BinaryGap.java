package codility;// you can also use imports, for example:
// import java.util.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class BinaryGap {
    public int solution(int N) {
        String binaryString = Integer.toBinaryString(N);
        char[] binaryChars = binaryString.toCharArray();
        List<Integer> gaps = new ArrayList<>();

        int gapCount = 0;
        for (char c : binaryChars) {
            if (c == '1') {
                if (gapCount != 0) gaps.add(gapCount);
                gapCount = 0;
            } else {
                gapCount++;
            }
        }

        int maxGap = 0;
        if (!gaps.isEmpty()) maxGap = Collections.max(gaps);
        return maxGap;
    }
}