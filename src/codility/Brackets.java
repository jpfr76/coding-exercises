package codility;// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

import java.util.Stack;

class Brackets {

  public int solution(String S) {
    if (S.isEmpty()) {
      return 1;
    }
    Stack<Character> stack = new Stack<>();
    char[] chars = S.toCharArray();
    for (char c : chars) {
      if (stack.isEmpty()) {
        switch (c) {
          case '}':
          case ']':
          case ')':
            return 0;
        }
      } else {
        char peekChar = stack.peek();
        switch (c) {
          case '}':
            if (peekChar != '{') {
              return 0;
            }
            stack.pop();
            break;
          case ']':
            if (peekChar != '[') {
              return 0;
            }
            stack.pop();
            break;
          case ')':
            if (peekChar != '(') {
              return 0;
            }
            stack.pop();
            break;
        }
      }
      switch (c) {
        case '{':
        case '[':
        case '(':
          stack.push(c);
          break;
      }
    }
    return stack.isEmpty() ? 1 : 0;
  }

  public static void main(String[] args) {
    System.out.println(new Brackets().solution("{{{{"));
  }
}