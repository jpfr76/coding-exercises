package codility;// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

import java.util.Arrays;

class RotateRight {
    public int[] solution(int[] A, int K) {
        int len = A.length;
        if (len == 0) return A;
        int startIdx = len - (K % len);
        int[] result = new int[len];
        for (int i = 0; i < len; i++) {
            result[i] = A[(startIdx + i) % len];
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(
            Arrays.toString(new RotateRight().solution(new int[] { 3, 8, 9, 7, 6 }, 8))
        );
    }
}