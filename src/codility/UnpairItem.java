package codility;// you can also use imports, for example:
// import java.util.*;

import java.util.HashMap;
import java.util.Map;

class UnpairItem {
    public int solution(int[] array) {
        Map<Integer, Integer> map = new HashMap<>();
        for (Integer item : array) map.put(item, map.getOrDefault(item, 0) + 1);
        for (Map.Entry<Integer, Integer> entrySet : map.entrySet()) {
            if ((entrySet.getValue() % 2) != 0) return entrySet.getKey();
        }
        return -1000000001;
    }
}