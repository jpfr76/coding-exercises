package codility;// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

import java.util.HashSet;
import java.util.Set;

class Distinct {
    public int solution(int[] A) {
        Set<Integer> items = new HashSet<>();
        for (int item : A) items.add(item);
        return items.size();
    }
}