package codility;// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class CountDiv {

  public int solution(int A, int B, int K) {
    int inclusive = (A % K == 0) ? 1 : 0;
    return inclusive + B / K - A / K;
  }

  public static void main(String[] args) {
    System.out.println(new CountDiv().solution(6, 11, 2));
  }
}