package codility;// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

import java.util.Arrays;

class MinDiff {
    public int solution(int[] A) {
        int len = A.length;
        int total = Arrays.stream(A).sum();

        int acc = 0;
        int minDiff = Integer.MAX_VALUE;
        for (int i = 0; i < len - 1; i++) {
            acc += A[i];
            int subTotal = Math.abs(acc - (total - acc));
            if (subTotal < minDiff) minDiff = subTotal;
        }
        return minDiff;
    }
}