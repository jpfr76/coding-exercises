package codility;// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class MinAvgSlice {

  private double avg(int[] prefixSum, int p, int q) {
    return (double) (prefixSum[q + 1] - prefixSum[p]) / (q - p + 1);
  }

  public int solution(int[] A) {
    int[] prefixSum = new int[A.length + 1];
    for (int i = 1; i <= A.length; i++) {
      prefixSum[i] = prefixSum[i - 1] + A[i - 1];
    }
    double minAvg = Double.MAX_VALUE;
    int minIdx = 0;
    for (int i = 0; i < A.length - 1; i++) {
      double avg = avg(prefixSum, i, i + 1);
      if (i < (A.length - 2)) {
        double avg3 = avg(prefixSum, i, i + 2);
        if (avg3 < avg) {
          avg = avg3;
        }
      }
      if (avg < minAvg) {
        minAvg = avg;
        minIdx = i;
      }
    }
    return minIdx;
  }

  public static void main(String[] args) {
    System.out.println(new MinAvgSlice().solution(new int[]{4, 2, 2, 5, 1, 5, 8}));
  }
}