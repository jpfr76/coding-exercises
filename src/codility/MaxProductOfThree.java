package codility;// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

import java.util.Arrays;

class MaxProductOfThree {

  public int solution(int[] A) {
    int len = A.length;
    if (len < 3) {
      return 0;
    }
    int[] sortedA = A.clone();
    Arrays.sort(sortedA);
    int maxPosProduct = sortedA[len - 1] * sortedA[len - 2];
    int maxNegProduct = sortedA[0] * sortedA[1];
    return maxNegProduct > maxPosProduct ? maxNegProduct * sortedA[len - 1] : maxPosProduct * sortedA[len -3];
  }
}
