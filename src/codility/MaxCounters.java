package codility;// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

import java.util.Arrays;

class MaxCounters {
    public int[] solution(int N, int[] A) {
        int[] counters = new int[N];
        Arrays.fill(counters, 0);
        int max = 0;
        int maxToSet = 0;
        for (int i = 0; i < A.length; i++) {
            if (A[i] == (N + 1)) {
                maxToSet = max;
            } else {
                if (counters[A[i]-1] < maxToSet) counters[A[i]-1] = maxToSet;
                counters[A[i]-1]++;
                if (counters[A[i]-1] > max) max = counters[A[i]-1];
            }
        }
        for (int j = 0; j < N; j++)
            if (counters[j] < maxToSet) counters[j] = maxToSet;
        return counters;
    }
}