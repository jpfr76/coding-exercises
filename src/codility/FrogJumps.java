package codility;

class FrogJumps {
    public int solution(int X, int Y, int D) {
        double d = D;
        double y = Y;
        double x = X;
        return (int) Math.ceil((y - x) / d);
    }
}