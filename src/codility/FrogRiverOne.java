package codility;// you can also use imports, for example:
// import java.util.*;


import java.util.HashSet;
import java.util.Set;

class FrogRiverOne {
    public int solution(int X, int[] A) {
        Set<Integer> set = new HashSet<>();
        for (int k = 1; k <= X; k++)
            set.add(k);
        for (int i = 0; i < A.length; i++) {
            set.remove(A[i]);
            if (set.isEmpty()) return i;
        }
        return -1;
    }
}