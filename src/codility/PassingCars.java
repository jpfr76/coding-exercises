package codility;// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class PassingCars {
    public int solution(int[] A) {
        Map<Integer, List<Integer>> map = new HashMap<>();

        for (int i = 0; i < A.length; i++) {
            int item = A[i];
            List<Integer> l = map.getOrDefault(item, new ArrayList<>());
            l.add(i);
            map.put(item, l);
        }
        return -1;
    }
}