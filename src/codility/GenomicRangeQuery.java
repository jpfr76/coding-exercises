package codility;// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

import java.util.Arrays;

class GenomicRangeQuery {

  public int[] solution(String S, int[] P, int[] Q) {
    int len = P.length;
    int[] result = new int[len];

    int[][] prefixSum = new int[3][S.length() + 1];

    for (int i = 0; i < S.length(); i++) {
      int a = 0, c = 0, g = 0;
      switch (S.charAt(i)) {
        case 'A':
          a = 1;
          break;
        case 'C':
          c = 1;
          break;
        case 'G':
          g = 1;
          break;
      }
      prefixSum[0][i + 1] = a + prefixSum[0][i];
      prefixSum[1][i + 1] = c + prefixSum[1][i];
      prefixSum[2][i + 1] = g + prefixSum[2][i];
    }

    for (int i = 0; i < len; i++) {
      int start = P[i];
      int end = Q[i] + 1;
      if ((prefixSum[0][end] - prefixSum[0][start]) > 0) {
        result[i] = 1;
      } else if ((prefixSum[1][end] - prefixSum[1][start]) > 0) {
        result[i] = 2;
      } else if ((prefixSum[2][end] - prefixSum[2][start]) > 0) {
        result[i] = 3;
      } else {
        result[i] = 4;
      }
    }

    return result;
  }

  public static void main(String[] args) {
    System.out.println(Arrays
        .toString(new GenomicRangeQuery().solution("AC", new int[]{0, 0, 1}, new int[]{0, 1, 1}))
    );
  }
}