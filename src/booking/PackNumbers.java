package booking;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class PackNumbers {
    // Complete the packNumbers function below.
    static List<String> packNumbers(List<Integer> arr) {
        List<String> result = new ArrayList<>();
        int count = 1;
        Integer prevNumber = arr.get(0);
        for (int i = 1; i < arr.size(); i++) {
            int n = arr.get(i);
            if (n == prevNumber) {
                count++;
            } else {
                add(result, count, prevNumber);
                count = 1;
                prevNumber = n;
            }
        }
        add(result, count, prevNumber);
        return result;
    }

    private static void add(List<String> result, int count, Integer prevNumber) {
        if (count > 1) {
            result.add(prevNumber + ":" + count);
        } else {
            result.add(Integer.toString(prevNumber));
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));

        int arrCount = Integer.parseInt(bufferedReader.readLine().trim());

        List<String> arrTemp = new ArrayList<>();

        IntStream.range(0, arrCount).forEach(i -> {
            try {
                arrTemp.add(bufferedReader.readLine().replaceAll("\\s+$", ""));
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        List<Integer> arr = arrTemp.stream()
            .map(String::trim)
            .map(Integer::parseInt)
            .collect(toList());

        List<String> res = packNumbers(arr);

        bufferedWriter.write(
            res.stream()
                .collect(joining("\n"))
            + "\n"
        );

        bufferedReader.close();
        bufferedWriter.close();
    }
}
