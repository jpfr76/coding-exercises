package booking;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class LuckyNumber {

    // Complete the getLuckyFloorNumber function below.
    static int getLuckyFloorNumber(int n) {
        int[] luckyNbrs = new int[n + 1];
        int luckyNbr = 0;
        for (int i = 1; i <= n; i++) {
            String luckyNbrStr;
            do {
                ++luckyNbr;
                luckyNbrStr = Integer.toString(luckyNbr);
            }
            while (luckyNbrStr.contains("4") || luckyNbrStr.contains("13"));
            luckyNbrs[i] = luckyNbr;
        }
        return luckyNbrs[n];
    }

    public static void main(String[] args) throws IOException {
        System.out.println(getLuckyFloorNumber(113));
    }
}