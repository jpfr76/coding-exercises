package practice;

import java.util.Arrays;
import java.util.Scanner;

public class TicTacToe {

  private static final String EMPTY = " ";
  private static final String PLAYER1 = "X";
  private static final String PLAYER2 = "O";

  private String currentPlayer;
  private String[] board;

  public TicTacToe() {
    currentPlayer = PLAYER1;
    setupBoard();
  }

  private void setupBoard() {
    board = new String[9];
    Arrays.fill(board, EMPTY);
  }

  private void init() {
    System.out.println("#######       WELCOME TO THE TIC TAC TOE        #######");
    System.out.println("####### X is the first player strating the game #######");
    System.out.println("#######    Please choose a number between 1 - 9 #######");
    System.out.println();
    setupBoard();
    do {
      printBoard();
      System.out.print("Enter Position (Player " + currentPlayer + "): ");
      Scanner input = new Scanner(System.in);
      int pos = input.nextInt();
      if (!validatePos(pos)) {
        System.out
            .println("!!!! Invalid Board Position !!!! Please choose a number between 1 and 9.");
        continue;
      }
      setPos(pos - 1);
      nextPlayer();
    } while (!isFinished());

    printBoard();

    if (isCompleted() && !existsWinner()) {
      System.out.println("It's a Draw!");
    } else {
      System.out.println("The winner is:" + getNextPlayer());
    }
  }

  private boolean isFinished() {
    boolean endGame = isCompleted();
    if (endGame) {
      return true;
    }
    return existsWinner();
  }

  private boolean existsWinner() {
    boolean endGame;// horizontals
    endGame = checkHorizontals();
    if (endGame) {
      return true;
    }

    endGame = checkVerticals();
    if (endGame) {
      return true;
    }

    return checkDiagonals();
  }

  private boolean isCompleted() {
    return Arrays.stream(board).noneMatch(p -> p.equals(EMPTY));
  }

  private boolean checkDiagonals() {
    if (board[4].equals(EMPTY)) {
      return false;
    }
    return board[0].equals(board[4]) && board[4].equals(board[8]) || // first diagonal
        board[2].equals(board[4]) && board[4].equals(board[6]);   // second diagonal
  }

  private boolean checkVerticals() {
    for (int i = 0; i < 3; i++) {
      if (board[i].equals(EMPTY) || board[i + 3].equals(EMPTY) || board[i + 6].equals(EMPTY)) {
        continue;
      }
      if (board[i].equals(board[i + 3]) && board[i + 3].equals(board[i + 6])) {
        return true;
      }
    }
    return false;
  }

  private boolean checkHorizontals() {
    for (int i = 0; i < 9; i += 3) {
      if (board[i].equals(EMPTY) || board[i + 1].equals(EMPTY) || board[i + 2].equals(EMPTY)) {
        continue;
      }
      if (board[i].equals(board[i + 1]) && board[i + 1].equals(board[i + 2])) {
        return true;
      }
    }
    return false;
  }

  private void setPos(int pos) {
    board[pos] = currentPlayer;
  }

  private boolean validatePos(int pos) {
    return pos > 0 && pos <= 9;
  }

  private void printBoard() {
    String[] str = new String[3];
    for (int i = 0, j = 0; i < board.length; i += 3, j++) {
      str[j] = "\t" + board[i] + " | " + board[i + 1] + " | " + board[i + 2];
    }
    System.out.println(String.join("\n--------------\n", str));
  }

  private void nextPlayer() {
    currentPlayer = getNextPlayer();
  }

  private String getNextPlayer() {
    return PLAYER1.equals(currentPlayer) ? PLAYER2 : PLAYER1;
  }

  public static void main(String[] args) {
    new TicTacToe().init();
  }
}
