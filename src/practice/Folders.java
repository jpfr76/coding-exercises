package practice;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Folders {
    public static Collection<String> folderNames(String xml, char startingLetter) throws Exception {
        List<String> result = new ArrayList<>();
        Pattern pattern = Pattern.compile("name[\\s]*=[\\s]*\""+ startingLetter + "([^\"]*)\"");
        Matcher matcher = pattern.matcher(xml);
        while (matcher.find()) {
            result.add(startingLetter + matcher.group(1));
        }
        return result;
    }
    
    public static void main(String[] args) throws Exception {
        String xml =
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<folder name=\"c\">" +
                    "<folder name=\"program files\">" +
                        "<folder name=\"uninstall information\" />" +
                    "</folder>" +
                    "<folder name=\"users\" />" +
                "</folder>";

        Collection<String> names = folderNames(xml, 'c');
        for(String name: names)
            System.out.println(name);
    }
}