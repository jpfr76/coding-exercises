package practice;

public class MultiplicationTable {

  private static void multiplicationTable(int x, int y) {
    for (int i = 1; i <= x; i++) {
      for (int j = 1; j <= y; j++) {
        int result = i * j;
        String resultStr = Integer.toString(result);
        if (resultStr.length() < 2) {
          resultStr = " " + resultStr;
        }
        System.out.print(resultStr);
        System.out.print("\t");
      }
      System.out.println();
    }
  }

  public static void main(String[] args) {
      multiplicationTable(5, 25);
  }
}
