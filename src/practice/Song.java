package practice;

import java.util.HashSet;

public class Song {
    private String name;
    private Song nextSong;

    public Song(String name) {
        this.name = name;
    }

    public void setNextSong(Song nextSong) {
        this.nextSong = nextSong;
    }

    public boolean isRepeatingPlaylist() {
        Song s = this;
        HashSet<Song> songs = new HashSet<>();
        while (s.nextSong != null) {
            if (songs.contains(s.nextSong)) return true;
            if (s.nextSong == this) return true;
            s = s.nextSong;
            songs.add(s);
        }
        return false;
    }

    public static void main(String[] args) {
        Song first = new Song("Hello");
        Song second = new Song("Eye of the tiger");

        first.setNextSong(second);
        second.setNextSong(first);

        System.out.println(first.isRepeatingPlaylist());
    }
}