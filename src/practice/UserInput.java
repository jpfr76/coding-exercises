package practice;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserInput {
    
    public static class TextInput {
        List<Character> value;

        public TextInput() {
            value = new ArrayList<>();
        }


        public void add(char c) {
            value.add(c);
        }

        public String getValue() {
            return value.stream().map(String::valueOf).collect(Collectors.joining());
        }
    }

    public static class NumericInput extends TextInput {
        @Override
        public void add(char c) {
            if (Character.isDigit(c)) super.add(c);
        }
    }

    public static void main(String[] args) {
        TextInput input = new NumericInput();
        input.add('1');
        input.add('a');
        input.add('0');
        System.out.println(input.getValue());
    }
}