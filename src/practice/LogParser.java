package practice;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogParser {
    public static Collection<Integer> getIdsByMessage(String xml, String message) throws Exception {
        List<Integer> result = new ArrayList<>();
        Pattern pattern = Pattern.compile(
                "<entry[\\s]+id[\\s]*=[\\s]*\"([^\"]+)\"[\\s]*>[\\s]*<message>" + message + "</message>[\\s]*</entry>"
        );
        Matcher matcher = pattern.matcher(xml);
        while (matcher.find()) {
            result.add(Integer.parseInt(matcher.group(1)));
        }
        return result;
    }
    
    public static void main(String[] args) throws Exception {
        String xml = 
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<log>\n" + 
                "    <entry id=\"1\">\n" + 
                "        <message>Application ended</message>\n" +
                "    </entry>\n" + 
                "    <entry id=\"2\">\n" + 
                "        <message>Application ended</message>\n" +
                "    </entry>\n" + 
                "</log>";
        
        Collection<Integer> ids = getIdsByMessage(xml, "Application ended");
        for(int id: ids)
            System.out.println(id); 
    }
}