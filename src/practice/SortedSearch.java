package practice;

public class SortedSearch {
    public static int countNumbers(int[] sortedArray, int lessThan) {
        int left = 0;
        int right = sortedArray.length - 1;
        while (left <= right) {
            int middle = (left + right) / 2;
            if (sortedArray[middle] < lessThan) {
                left = middle + 1;
            } else if (sortedArray[middle] > lessThan) {
                right = middle - 1;
            } else {
                return middle;
            }
        }
        return left;
    }
    
    public static void main(String[] args) {
        System.out.println(SortedSearch.countNumbers(new int[] {  1, 2, 3, 5, 6, 8, 9 }, 4));
    }
}