package practice;

import java.util.Arrays;
import java.util.HashSet;

public class MergeNames {
    
    public static String[] uniqueNames(String[] names1, String[] names2) {
        HashSet<String> hash = new HashSet<>();
        hash.addAll(Arrays.asList(names1));
        hash.addAll(Arrays.asList(names2));
        return hash.toArray(new String[0]);
    }
    
    public static void main(String[] args) {
        String[] names1 = new String[] {"Ava", "Emma", "Olivia"};
        String[] names2 = new String[] {"Olivia", "Sophia", "Emma"};
        System.out.println(String.join(", ", MergeNames.uniqueNames(names1, names2))); // should print Ava, Emma, Olivia, Sophia
    }
}