package practice;

import java.util.UUID;

public class TestDome {

    public static void main(String[] args) {
        AlertService service = new AlertService(new MapAlertDAO());
        UUID c = service.raiseAlert();
        System.out.println(service.getAlertTime(c));
    }
}
