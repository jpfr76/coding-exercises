package practice;

import java.util.HashSet;
import java.util.Set;

public class Anagram {
  public static boolean areAnagrams(String str1, String str2) {
    if (str1.length() != str2.length()) return false;
    char[] c1 = str1.toLowerCase().toCharArray();
    char[] c2 = str2.toLowerCase().toCharArray();
    int len = 'z' - 'a' + 1;
    int[] counters = new int[len];
    for (int i = 0; i < c1.length; i++) {
      counters[c1[i] - 'a']++;
      counters[c2[i] - 'a']--;
    }
    for (int i = 0; i < len; i++) {
      if (counters[i] != 0) {
        return false;
      }
    }
    return true;
  }

  public static void main(String[] args) {
    System.out.println(areAnagrams("babac", "ababa"));
  }
}
