package practice;

import java.util.HashMap;
import java.util.HashSet;

public class TwoSum {
    public static int[] findTwoSum(int[] list, int sum) {
        HashMap<Integer, HashSet<Integer>> map = new HashMap<>();
        for (int i = 0; i < list.length; i++) {
            if (!map.containsKey(list[i])) map.put(list[i], new HashSet<>());
            HashSet<Integer> set = map.get(list[i]);
            set.add(i);
        }

        for (int i = 0; i < list.length; i++) {
            int possible = sum - list[i];
            if (possible > 0 && map.containsKey(possible) && (map.get(possible).size() > 1 || !map.get(possible).contains(i))) {
                int j = -1;
                for (Integer k : map.get(possible)) {
                    if (k != i) {
                        j = k;
                        break;
                    }
                }
                if (j == -1) continue;

                return new int[] { i, j};
            }
        }

        return null;
    }

    public static void main(String[] args) {
        int[] indices = findTwoSum(new int[] { 3, 1, 5, 7, 5, 9 }, 10);
        if (indices != null) {
            System.out.println(indices[0] + " " + indices[1]);
        }
    }
}