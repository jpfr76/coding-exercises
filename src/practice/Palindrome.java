package practice;

public class Palindrome {
    public static boolean isPalindrome(String word) {
        String w = word.toLowerCase();
        char[] charSeq = w.toCharArray();
        int len = charSeq.length;
        if (len == 1) return true;
        int middle = len / 2;
        for (int i = 0; i < middle; i++) {
             if (charSeq[i] != charSeq[len-1-i]) {
                 return false;
             }
        }
        return true;
    }
    
    public static void main(String[] args) {
        System.out.println(Palindrome.isPalindrome("Deleveled"));
    }
}