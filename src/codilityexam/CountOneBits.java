package codilityexam;// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class CountOneBits {

  public int solution(int A, int B) {
    int product = A * B;
    String productBinaryStr = Integer.toBinaryString(product);
    int len = productBinaryStr.length();
    int totalOneBits = 0;
    for (int i = 0; i < len; i++) {
        if (productBinaryStr.charAt(i) == '1') {
            totalOneBits++;
        }
    }
    return totalOneBits;
  }

    public static void main(String[] args) {
        System.out.println(new CountOneBits().solution(5, 177));
    }
}