package codilityexam;// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class MaxDiff {

  //hum... sounds a little bit weird this solution :) let's try
  //so simple? hum cannot be, trying to find some corner cases
  //ok this is wrong because max and min can be in the same array
  public int solution3(int[] A) {
    int len = A.length;
    if (len == 1) {
      return A[0];
    }

    int minValue = Integer.MAX_VALUE;
    int maxValue = Integer.MIN_VALUE;
    for (int value : A) {
      if (value > maxValue) {
        maxValue = value;
      }
      if (value < minValue) {
        minValue = value;
      }
    }

    return maxValue - minValue;
  }

  public int solution2(int[] A) {
    int len = A.length;
    if (len == 1) {
      return A[0];
    }
    int maxDiff = A[1] - A[0]; //starting point
    int minValue = A[0]; //let's assume that the first element is the most minimum one

    //TODO: check what happens if the min value appears after max value
    for (int i = 2; i < len; i++) {
      int value = A[i];
      int possibleMaxDiff = (value - minValue);
      if (possibleMaxDiff > maxDiff) {
        maxDiff = possibleMaxDiff;
      }
      if (value < minValue) {
        minValue = value;
      }
    }
    return maxDiff;
  }

  //so basically we need to have 2 arrays left and right with the max values of each of them
  //it's important that we store the max value that exists until the index i
  //the right array it's important that we store the max value in a reverse order
  public int solution(int[] A) {
    int len = A.length;
    if (len == 1) {
      return A[0];
    }

    //fill up left array with max values (using a variant of the prefix sums strategy)
    int[] leftPrefixMax = new int[len];
    int maxLeft = A[0];
    leftPrefixMax[0] = maxLeft;
    for (int i = 1; i < len; i++) {
      int value = A[i];
      if (value > maxLeft) {
        maxLeft = value;
      }
      leftPrefixMax[i] = maxLeft;
    }

    //fill up right array with max values (using a variant of the prefix sums strategy)
    //the right array should be filled in a reverse way => this is necessary to then do the diff in a more simple way
    int[] rightPrefixMax = new int[len];
    int maxRight = A[len - 1];
    rightPrefixMax[len - 1] = maxRight;
    for (int i = len - 2; i >= 0; i--) {
      int value = A[i];
      if (value > maxRight) {
        maxRight = value;
      }
      rightPrefixMax[i] = maxRight;
    }

    int maxDiff = 0;
    for (int k = 0; k < len - 1; k++) {
      int diff = Math.abs(leftPrefixMax[k] - rightPrefixMax[k + 1]);
      if (diff > maxDiff) {
        maxDiff = diff;
      }
    }
    return maxDiff;
  }

  public static void main(String[] args) {
    System.out.println(new MaxDiff().solution(new int[]{1, 3, -3}));
  }

}