package sonarsource;

import java.util.Optional;

public class MultiplyCommandImpl implements CommandStrategy {

  @Override
  public Optional<Integer> process(int number1, int number2) {
    return Optional.of(number1 * number2);
  }
}
