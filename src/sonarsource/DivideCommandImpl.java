package sonarsource;

import java.util.Optional;

public class DivideCommandImpl implements CommandStrategy {

  @Override
  public Optional<Integer> process(int number1, int number2) throws CommandException {
    if (number2 == 0) {
      throw new CommandException("Cannot divide by zero. Please choose another number.");
    }
    return Optional.of(number1 / number2);
  }
}
