package sonarsource;

import java.util.Optional;

public class AddCommandImpl implements CommandStrategy {

  @Override
  public Optional<Integer> process(int number1, int number2) {
    return Optional.of(number1 + number2);
  }
}
