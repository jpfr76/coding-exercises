package sonarsource;

import java.util.Optional;

public interface CommandStrategy {
  Optional<Integer> process(int number1, int number2) throws CommandException;
}
