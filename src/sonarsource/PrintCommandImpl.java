package sonarsource;

import java.util.Optional;

public class PrintCommandImpl implements CommandStrategy {

  @Override
  public Optional<Integer> process(int number1, int number2) {
    System.out.println(number1);
    return Optional.empty();
  }
}
