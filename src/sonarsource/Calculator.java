package sonarsource;

import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.Stack;

public class Calculator {

  private Map<String, CommandStrategy> commandStrategies;
  private Stack<Integer> stack;

  public Calculator(Map<String, CommandStrategy> commandStrategies) {
    this.commandStrategies = commandStrategies;
    this.stack = new Stack<>();
    this.stack.push(0);
  }

  public int getCurrentValue() {
    return stack.peek();
  }

  private void print() {
    stack.stream().skip(1).forEach(System.out::println);
  }

  public void parse(String command) {
    command = command.toLowerCase();
    String[] commandSplit = command.split(" ");
    int n = commandSplit.length > 1 ? Integer.parseInt(commandSplit[1]) : 0;
    String operator = commandSplit[0];
    //validate();
    process(operator, n);
  }

  private void process(String operator, int n) {
    if (operator.equals("print")) {
      print();
      return;
    }
    CommandStrategy operatorStrategy = commandStrategies.get(operator);
    try {
      Optional<Integer> result = operatorStrategy.process(getCurrentValue(), n);
      result.ifPresent(integer -> stack.push(integer));
    } catch (CommandException e) {
      System.out.println(e.getMessage());
    }
  }

  public void init() {
    while (true) {
      Scanner in = new Scanner(System.in);
      parse(in.nextLine());
    }
  }

  public static void main(String[] args) {
    System.out.println("####### WELCOME TO THE SONAR SOURCE CALCULATOR ######");
    Map<String, CommandStrategy> strategyMap = new HashMap<>();
    strategyMap.put("add", new AddCommandImpl());
    strategyMap.put("sub", new SubCommandImpl());
    strategyMap.put("multiply by", new MultiplyCommandImpl());
    strategyMap.put("divide by", new DivideCommandImpl());
    Calculator c = new Calculator(strategyMap);
    c.init();
  }
}
